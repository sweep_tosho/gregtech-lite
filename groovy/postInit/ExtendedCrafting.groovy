import net.minecraftforge.oredict.OreDictionary

// Downstream Processing for Singularities and End game goals.

// Extended Crafting Material Compatibilities, GT Format material init at `GTLiteModCompatibilityMaterials` class.
ore('plateBlackIron').add(item('extendedcrafting:material', 2))
ore('stickBlackIron').add(item('extendedcrafting:material', 3))

// Crystaltine Ingot
mods.extendedcrafting.table_crafting.removeByOutput(item('extendedcrafting:material', 24))
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('         ',
            '         ',
            'JJJJJJJJJ',
            'JICCICCIJ',
            'JCIICIICJ',
            'JICCICCIJ',
            'JJJJJJJJJ',
            '         ',
            '         ')
    .key('C', item('extendedcrafting:material', 11))
    .key('I', ore('ingotCrystalMatrix'))
    .key('J', ore('ingotCrystallineAlloy'))
    .output(item('extendedcrafting:material', 24))
    .tierUltimate()
    .register()

// Quantum Compressor
crafting.remove('extendedcrafting:compressor')
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('DADIBIDAD',
            'I P C P I',
            'BMEMCMEMB',
            'A PXNXP A',
            'ACCNONCCA',
            'A PXNXP A',
            'BMEMCMEMB',
            'I P C P I',
            'DADIBIDAD')
    .key('A', ore('plateDenseCosmicNeutronium'))
    .key('B', ore('plateIrradiantReinforcedNeutronium'))
    .key('C', ore('plateBlackIron')) // ExtendedCrafting Black Iron (Not Black Steel in GregTech).
    .key('D', ore('blockCosmicNeutronium'))
    .key('E', metaitem('electric.motor.uhv'))
    .key('M', metaitem('conveyor.module.uhv'))
    .key('N', ore('plateDenseNeutronium'))
    .key('I', ore('ingotCrystaltine')) // ExtendedCrafting Crystaltine (Not Crystalline Alloy in GregTech Lite Core/Ender IO).
    .key('O', metaitem('compressor.uhv'))
    .key('P', metaitem('electric.piston.uhv'))
    .key('X', ore('circuitUev'))
    .output(item('extendedcrafting:compressor'))
    .tierUltimate()
    .register()

// Add Ore Dict Formatting for all ExtendedCrafting Singularities.
// TODO Add a Protection Mechanism in GregTech Lite Core, if player do not like ExtendedCrafting and delete it,
//      then add Singularities internal for required in game stage (we added `singularity` Ore Prefix internally,
//      but not it is deprecated).

// Eternitic Singularity Components.
ore('singularityLapis').add(item('extendedcrafting:singularity', 2))
ore('singularityGold').add(item('extendedcrafting:singularity', 5))
ore('singularitySilver').add(item('extendedcrafting:singularity', 22))
ore('singularityIron').add(item('extendedcrafting:singularity', 1))
ore('singularityRedstone').add(item('extendedcrafting:singularity', 3))
ore('singularityTin').add(item('extendedcrafting:singularity', 18))
ore('singularityLead').add(item('extendedcrafting:singularity', 23))
ore('singularityCopper').add(item('extendedcrafting:singularity', 17))
ore('singularityNetherQuartz').add(item('extendedcrafting:singularity_custom', 40))
ore('singularityNickel').add(item('extendedcrafting:singularity', 25))
ore('singularityEnderium').add(item('extendedcrafting:singularity', 50))
ore('singularityCoal').add(item('extendedcrafting:singularity', 0))
ore('singularityDiamond').add(item('extendedcrafting:singularity', 6))
ore('singularityEmerald').add(item('extendedcrafting:singularity', 7))
ore('singularityCharcoal').add(item('extendedcrafting:singularity_custom', 41))
ore('singularityAluminium').add(item('extendedcrafting:singularity', 16))
ore('singularityBrass').add(item('extendedcrafting:singularity', 21))
ore('singularityBronze').add(item('extendedcrafting:singularity', 19))
ore('singularityElectrum').add(item('extendedcrafting:singularity', 27))
ore('singularityInvar').add(item('extendedcrafting:singularity', 28))
ore('singularityGlowstone').add(item('extendedcrafting:singularity', 4))
ore('singularityOsmium').add(item('extendedcrafting:singularity_custom', 42))
ore('singularityOlivine').add(item('extendedcrafting:singularity_custom', 43))
ore('singularityRuby').add(item('extendedcrafting:singularity_custom', 44))
ore('singularitySapphire').add(item('extendedcrafting:singularity_custom', 45))
ore('singularitySteel').add(item('extendedcrafting:singularity', 24))
ore('singularityTitanium').add(item('extendedcrafting:singularity', 31))
ore('singularityTungsten').add(item('extendedcrafting:singularity', 30))
ore('singularityUranium').add(item('extendedcrafting:singularity', 32))
ore('singularityZinc').add(item('extendedcrafting:singularity', 20))
ore('singularityIndium').add(item('extendedcrafting:singularity_custom', 46))
ore('singularityPalladium').add(item('extendedcrafting:singularity_custom', 47))
ore('singularityDamascusSteel').add(item('extendedcrafting:singularity_custom', 48))
ore('singularityBlackSteel').add(item('extendedcrafting:singularity', 49))
ore('singularityFluxedElectrum').add(item('extendedcrafting:singularity', 50))
ore('singularitySalt').add(item('extendedcrafting:singularity', 51))
ore('singularityVoidMetal').add(item('extendedcrafting:singularity', 52))
ore('singularityIridium').add(item('extendedcrafting:singularity', 35))
ore('singularityNetherStar').add(item('extendedcrafting:singularity_custom', 53))
ore('singularityPlatinum').add(item('extendedcrafting:singularity', 34))
ore('singularityNaquadria').add(item('extendedcrafting:singularity_custom', 54))
ore('singularityPlutonium').add(item('extendedcrafting:singularity_custom', 55))
ore('singularityOrundum').add(item('extendedcrafting:singularity_custom', 56))
ore('singularityQuantium').add(item('extendedcrafting:singularity_custom', 57))
ore('singularityEuropium').add(item('extendedcrafting:singularity_custom', 58))
ore('singularityRhugnor').add(item('extendedcrafting:singularity_custom', 59))
ore('singularityHypogen').add(item('extendedcrafting:singularity_custom', 60))
ore('singularityConductiveIron').add(item('extendedcrafting:singularity_custom', 61))
ore('singularityElectricalSteel').add(item('extendedcrafting:singularity_custom', 62))
ore('singularityEnergeticAlloy').add(item('extendedcrafting:singularity_custom', 63))
ore('singularityDarkSteel').add(item('extendedcrafting:singularity_custom', 64))
ore('singularityPulsatingIron').add(item('extendedcrafting:singularity_custom', 65))
ore('singularityRedstoneAlloy').add(item('extendedcrafting:singularity_custom', 66))
ore('singularitySoularium').add(item('extendedcrafting:singularity_custom', 67))
ore('singularityVibrantAlloy').add(item('extendedcrafting:singularity_custom', 68))
ore('singularityNeutronium').add(item('extendedcrafting:singularity_custom', 69))
ore('singularityTaranium').add(item('extendedcrafting:singularity_custom', 70))
ore('singularityPotin').add(item('extendedcrafting:singularity_custom', 71))
ore('singularityObsidian').add(item('extendedcrafting:singularity_custom', 72))
ore('singularityTiberium').add(item('extendedcrafting:singularity_custom', 73))
ore('singularityCobalt').add(item('extendedcrafting:singularity', 65))
ore('singularityEnderPearl').add(item('extendedcrafting:singularity_custom', 74))
ore('singularityFluix').add(item('extendedcrafting:singularity_custom', 75))

// Chronic Singularity Components.
// TODO

// The Basic Processing of Eternity Singularity referenced to recipes of same name item in GT: New Horizons Modpack.
// In original recipe, Black Plutonium block is one of basic Block Material, we change it to another material because
// the original material is from GalaxySpace Mod, but in gtlitecore, Black Plutonium is an Alloy Material. We should
// choose a more exotic material. Another problem is we use Eternity Singularity beyond UXV stage, but in GTNH, this
// item is used for UHV+ stages. Both of the two reasons, we tweak some materials, and then player should cost SpNt
// and Black Dwarf Matter as basic material, and Bedrock block as sub-material in 1st and 2nd Step of Processing.

// In gtlitecore, we has two Ultimate Singularities: Eternitic Singularity (same as Eternity Singularity in GTNH),
// and Chronic Singularity (used for some GregTech interaction situation).

// Eternitic Singularity
// Components of Eternity Singularity:
// Nitronic Singularity: Lapis, Gold, Silver, Iron, Redstone, Tin, Lead, Copper, Nether Quartz;
// Psyychotic Singularity: Nickel, Enderium, Coal, Diamond, Emerald, Charcoal, Aluminium, Brass, Bronze;
// Fantastic Singularity: Electrum, Invar, Glowstone, Osmium, Olivine, Ruby, Sapphire, Steel, Titanium;
// Pneumatic Singularity: Tungsten, Uranium, Zinc, Indium, Palladium, Damascus Steel, Black Steel, Fluxed Electrum, Salt;
// Cryptic Singularity: Void Metal, Iridium, Nether Star, Platinum, Naquadria, Plutonium, Orundum, Quantium, Europium;
// Historic Singularity: Rhugnor, Hypogen, Conductive Iron, Electrical Steel, Energetic Alloy, Dark Steel, Pulsating Iron, Redstone Alloy, Soularium;
// Meteoric Singularity: Vibrant Alloy, Neutronium, Taranium, Potin, Obsidian, Tiberium, Cobalt, Ender Pearl, Fluix.
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('     N   ',
            '  MM  M  ',
            ' M  A  M ',
            'N  BNC M ',
            '  NNDNN  ',
            ' M ENF  N',
            ' M  G  M ',
            '  M  MM  ',
            '   N     ')
    .key('N', ore('blockCosmicNeutronium'))
    .key('M', ore('blockBlackDwarfMatter'))
    .key('A', item('gtlitecore:singularity_nitronic'))
    .key('B', item('gtlitecore:singularity_psychotic'))
    .key('C', item('gtlitecore:singularity_fantastic')) // In GTNH, they choose Sphaghettic Singularity, but we choose Fantastic Singularity.
    .key('D', item('gtlitecore:singularity_pneumatic'))
    .key('E', item('gtlitecore:singularity_cryptic'))
    .key('F', item('gtlitecore:singularity_historic'))
    .key('G', item('gtlitecore:singularity_meteoric'))
    .output(item('gtlitecore:singularity_eternitic'))
    .tierUltimate()
    .register()

// Chronic Singularity
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('     N   ',
            '  MM  M  ',
            ' M  A  M ',
            'N  BNC M ',
            '  NNDNN  ',
            ' M ENF  N',
            ' M  G  M ',
            '  M  MM  ',
            '   N     ')
    .key('N', ore('blockNeutronium'))
    .key('M', ore('blockWhiteDwarfMatter'))
    .key('A', item('gtlitecore:singularity_angelic'))
    .key('B', item('gtlitecore:singularity_chromatic'))
    .key('C', item('gtlitecore:singularity_prismatic'))
    .key('D', item('gtlitecore:singularity_robotic'))
    .key('E', item('gtlitecore:singularity_galactic'))
    .key('F', item('gtlitecore:singularity_hydrolic'))
    .key('G', item('gtlitecore:singularity_geologic'))
    .output(item('gtlitecore:singularity_chronic'))
    .tierUltimate()
    .register()

// Nitronic Singularity
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('         ',
            '  MM  M  ',
            ' M  A  M ',
            '   BNC M ',
            '  DNENF  ',
            ' M GNH   ',
            ' M  I  M ',
            '  M  MM  ',
            '         ')
    .key('N', ore('blockPeriodicium'))
    .key('M', ore('blockBedrock'))
    .key('A', ore('singularityLapis'))
    .key('B', ore('singularityGold'))
    .key('C', ore('singularitySilver'))
    .key('D', ore('singularityIron'))
    .key('E', ore('singularityRedstone'))
    .key('F', ore('singularityTin'))
    .key('G', ore('singularityLead'))
    .key('H', ore('singularityCopper'))
    .key('I', ore('singularityNetherQuartz'))
    .output(item('gtlitecore:singularity_nitronic'))
    .tierUltimate()
    .register()

// Psychotic Singularity
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('         ',
            '  MM  M  ',
            ' M  A  M ',
            '   BNC M ',
            '  DNENF  ',
            ' M GNH   ',
            ' M  I  M ',
            '  M  MM  ',
            '         ')
    .key('N', ore('blockPeriodicium'))
    .key('M', ore('blockBedrock'))
    .key('A', ore('singularityNickel'))
    .key('B', ore('singularityEnderium'))
    .key('C', ore('singularityCoal'))
    .key('D', ore('singularityDiamond'))
    .key('E', ore('singularityEmerald'))
    .key('F', ore('singularityCharcoal'))
    .key('G', ore('singularityAluminium'))
    .key('H', ore('singularityBrass'))
    .key('I', ore('singularityBronze'))
    .output(item('gtlitecore:singularity_psychotic'))
    .tierUltimate()
    .register()

// Fantastic Singularity
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('         ',
            '  MM  M  ',
            ' M  A  M ',
            '   BNC M ',
            '  DNENF  ',
            ' M GNH   ',
            ' M  I  M ',
            '  M  MM  ',
            '         ')
    .key('N', ore('blockPeriodicium'))
    .key('M', ore('blockBedrock'))
    .key('A', ore('singularityElectrum'))
    .key('B', ore('singularityInvar'))
    .key('C', ore('singularityGlowstone')) // Original: Magnesium Singularity, Current: Glowstone Singularity
    .key('D', ore('singularityOsmium'))
    .key('E', ore('singularityOlivine'))
    .key('F', ore('singularityRuby'))
    .key('G', ore('singularitySapphire'))
    .key('H', ore('singularitySteel'))
    .key('I', ore('singularityTitanium'))
    .output(item('gtlitecore:singularity_fantastic'))
    .tierUltimate()
    .register()

// Pneumatic Singularity
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('         ',
            '  MM  M  ',
            ' M  A  M ',
            '   BNC M ',
            '  DNENF  ',
            ' M GNH   ',
            ' M  I  M ',
            '  M  MM  ',
            '         ')
    .key('N', ore('blockPeriodicium'))
    .key('M', ore('blockBedrock'))
    .key('A', ore('singularityTungsten'))
    .key('B', ore('singularityUranium'))
    .key('C', ore('singularityZinc'))
    .key('D', ore('singularityIndium')) // Original: Calcium Phosphate Singularity, Current: Indium Singularity
    .key('E', ore('singularityPalladium'))
    .key('F', ore('singularityDamascusSteel'))
    .key('G', ore('singularityBlackSteel'))
    .key('H', ore('singularityFluxedElectrum'))
    .key('I', ore('singularitySalt')) // Original: Mercury Singularity, Current: Salt Singularity
    .output(item('gtlitecore:singularity_pneumatic'))
    .tierUltimate()
    .register()

// Cryptic Singularity
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('         ',
            '  MM  M  ',
            ' M  A  M ',
            '   BNC M ',
            '  DNENF  ',
            ' M GNH   ',
            ' M  I  M ',
            '  M  MM  ',
            '         ')
    .key('N', ore('blockPeriodicium'))
    .key('M', ore('blockBedrock'))
    .key('A', ore('singularityVoidMetal')) // Original: Shadow Iron Singularity, Current: Void Metal Singularity
    .key('B', ore('singularityIridium'))
    .key('C', ore('singularityNetherStar'))
    .key('D', ore('singularityPlatinum'))
    .key('E', ore('singularityNaquadria'))
    .key('F', ore('singularityPlutonium'))
    .key('G', ore('singularityOrundum')) // Original: Meteoric Iron Singularity, Current: Orundum Singularity
    .key('H', ore('singularityQuantium')) // Original: Desh Singularity, Current: Quantium Singularity
    .key('I', ore('singularityEuropium'))
    .output(item('gtlitecore:singularity_cryptic'))
    .tierUltimate()
    .register()

// Historic Singularity
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('         ',
            '  MM  M  ',
            ' M  A  M ',
            '   BNC M ',
            '  DNENF  ',
            ' M GNH   ',
            ' M  I  M ',
            '  M  MM  ',
            '         ')
    .key('N', ore('blockPeriodicium'))
    .key('M', ore('blockBedrock'))
    .key('A', ore('singularityRhugnor')) // Original: Draconium Singularity, Current: Rhugnor Singularity
    .key('B', ore('singularityHypogen')) // Original: Awakened Draconium Singularity, Current: Hypogen Singularity
    .key('C', ore('singularityConductiveIron'))
    .key('D', ore('singularityElectricalSteel'))
    .key('E', ore('singularityEnergeticAlloy'))
    .key('F', ore('singularityDarkSteel'))
    .key('G', ore('singularityPulsatingIron'))
    .key('H', ore('singularityRedstoneAlloy'))
    .key('I', ore('singularitySoularium'))
    .output(item('gtlitecore:singularity_historic'))
    .tierUltimate()
    .register()

// Meteoric Singularity
mods.extendedcrafting.table_crafting.shapedBuilder()
    .matrix('         ',
            '  MM  M  ',
            ' M  A  M ',
            '   BNC M ',
            '  DNENF  ',
            ' M GNH   ',
            ' M  I  M ',
            '  M  MM  ',
            '         ')
    .key('N', ore('blockPeriodicium'))
    .key('M', ore('blockBedrock'))
    .key('A', ore('singularityVibrantAlloy'))
    .key('B', ore('singularityNeutronium')) // Original: Unstable Metal Singularity, Current: Neutronium Singularity
    .key('C', ore('singularityTaranium')) // Original: Electrotine Singularity, Current: Taranium Singularity
    .key('D', ore('singularityPotin')) // Original: Aluminium Brass Singularity, Current: Potin Singularity
    .key('E', ore('singularityObsidian')) // Original: Alumite Singularity, Current: Obsidian Singularity
    .key('F', ore('singularityTiberium')) // Original: Ardite Singularity, Current: Tiberium Singularity
    .key('G', ore('singularityCobalt'))
    .key('H', ore('singularityEnderPearl')) // Original: Ender Singularity, Current: Ender Pearl Singularity
    .key('I', ore('singularityFluix')) // Original: Manyullyn Singularity, Current: Fluix Singularity
    .output(item('gtlitecore:singularity_meteoric'))
    .tierUltimate()
    .register()

// TODO Chronic Singularity Component big singularities.

// Remove all Quantum Compressor recipes.
mods.extendedcrafting.compression_crafting.removeAll()

// Lapis Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockLapis'))
    .inputCount(1215)
    .output(item('extendedcrafting:singularity:2'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Gold Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockGold'))
    .inputCount(1215)
    .output(item('extendedcrafting:singularity:5'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Silver Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockSilver'))
    .inputCount(7296)
    .output(item('extendedcrafting:singularity:22'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Iron Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockIron'))
    .inputCount(7296)
    .output(item('extendedcrafting:singularity:1'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Redstone Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockRedstone'))
    .inputCount(7296)
    .output(item('extendedcrafting:singularity:3'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Tin Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockTin'))
    .inputCount(3648)
    .output(item('extendedcrafting:singularity:18'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Lead Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockLead'))
    .inputCount(3648)
    .output(item('extendedcrafting:singularity:23'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Copper Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockCopper'))
    .inputCount(3648)
    .output(item('extendedcrafting:singularity:17'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Nether Quartz Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockNetherQuartz'))
    .inputCount(1215)
    .output(item('extendedcrafting:singularity_custom:40'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Nickel Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockNickel'))
    .inputCount(3648)
    .output(item('extendedcrafting:singularity:25'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Enderium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockEnderium'))
    .inputCount(608)
    .output(item('extendedcrafting:singularity:50'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Coal Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockCoal'))
    .inputCount(3648)
    .output(item('extendedcrafting:singularity:0'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Diamond Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockDiamond'))
    .inputCount(729)
    .output(item('extendedcrafting:singularity:6'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Emerald Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockEmerald'))
    .inputCount(729)
    .output(item('extendedcrafting:singularity:7'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Charcoal Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockCharcoal'))
    .inputCount(7296)
    .output(item('extendedcrafting:singularity_custom:41'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Aluminium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockAluminium'))
    .inputCount(1824)
    .output(item('extendedcrafting:singularity:16'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Brass Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockBrass'))
    .inputCount(1824)
    .output(item('extendedcrafting:singularity:21'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Bronze Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockBronze'))
    .inputCount(1824)
    .output(item('extendedcrafting:singularity:19'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Electrum Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockElectrum'))
    .inputCount(912)
    .output(item('extendedcrafting:singularity:27'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Invar Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockInvar'))
    .inputCount(1824)
    .output(item('extendedcrafting:singularity:28'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Glowstone Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockGlowstone'))
    .inputCount(3648)
    .output(item('extendedcrafting:singularity:4'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Osmium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockOsmium'))
    .inputCount(406)
    .output(item('extendedcrafting:singularity_custom:42'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Olivine Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockOlivine'))
    .inputCount(608)
    .output(item('extendedcrafting:singularity_custom:43'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Ruby Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockRuby'))
    .inputCount(608)
    .output(item('extendedcrafting:singularity_custom:44'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Sapphire Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockSapphire'))
    .inputCount(608)
    .output(item('extendedcrafting:singularity_custom:45'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Steel Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockSteel'))
    .inputCount(912)
    .output(item('extendedcrafting:singularity:24'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Titanium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockTitanium'))
    .inputCount(2024)
    .output(item('extendedcrafting:singularity:31'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Tungsten Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockTungsten'))
    .inputCount(244)
    .output(item('extendedcrafting:singularity:30'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Uranium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockUranium'))
    .inputCount(507)
    .output(item('extendedcrafting:singularity:32'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Zinc Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockZinc'))
    .inputCount(3648)
    .output(item('extendedcrafting:singularity:20'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Indium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockIndium'))
    .inputCount(365)
    .output(item('extendedcrafting:singularity_custom:46'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Palladium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockPalladium'))
    .inputCount(136)
    .output(item('extendedcrafting:singularity_custom:47'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Damascus Steel Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockDamascusSteel'))
    .inputCount(153)
    .output(item('extendedcrafting:singularity_custom:48'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Black Steel Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockBlackSteel'))
    .inputCount(304)
    .output(item('extendedcrafting:singularity_custom:49'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Fluxed Electrum Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockFluxedElectrum'))
    .inputCount(16)
    .output(item('extendedcrafting:singularity_custom:50'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Salt Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockSalt'))
    .inputCount(1824)
    .output(item('extendedcrafting:singularity_custom:51'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Void Metal Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockVoidMetal'))
    .inputCount(406)
    .output(item('extendedcrafting:singularity_custom:52'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Iridium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockIridium'))
    .inputCount(62)
    .output(item('extendedcrafting:singularity:35'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Nether Star Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockNetherStar'))
    .inputCount(512)
    .output(item('extendedcrafting:singularity_custom:53'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Platinum Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockPlatinum'))
    .inputCount(406)
    .output(item('extendedcrafting:singularity:34'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Naquadria Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockNaquadria'))
    .inputCount(66)
    .output(item('extendedcrafting:singularity_custom:54'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Plutonium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockPlutonium'))
    .inputCount(244)
    .output(item('extendedcrafting:singularity_custom:55'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Orundum Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockOrundum'))
    .inputCount(912)
    .output(item('extendedcrafting:singularity_custom:56'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Quantium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockQuantium'))
    .inputCount(203)
    .output(item('extendedcrafting:singularity_custom:57'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Europium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockEuropium'))
    .inputCount(62)
    .output(item('extendedcrafting:singularity_custom:58'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Rhugnor Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockRhugnor'))
    .inputCount(1296)
    .output(item('extendedcrafting:singularity_custom:59'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Hypogen Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockHypogen'))
    .inputCount(760)
    .output(item('extendedcrafting:singularity_custom:60'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Conductive Iron Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockConductiveIron'))
    .inputCount(912)
    .output(item('extendedcrafting:singularity_custom:61'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Electrical Steel Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockElectricalSteel'))
    .inputCount(912)
    .output(item('extendedcrafting:singularity_custom:62'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Energetic Alloy Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockEnergeticAlloy'))
    .inputCount(191)
    .output(item('extendedcrafting:singularity_custom:63'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Dark Steel Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockDarkSteel'))
    .inputCount(912)
    .output(item('extendedcrafting:singularity_custom:64'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Pulsating Iron Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockPulsatingIron'))
    .inputCount(912)
    .output(item('extendedcrafting:singularity_custom:65'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Redstone Alloy Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockRedstoneAlloy'))
    .inputCount(912)
    .output(item('extendedcrafting:singularity_custom:66'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Soularium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockSoularium'))
    .inputCount(456)
    .output(item('extendedcrafting:singularity_custom:67'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Vibrant Alloy Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockVibrantAlloy'))
    .inputCount(145)
    .output(item('extendedcrafting:singularity_custom:68'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Neutronium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockNeutronium'))
    .inputCount(66)
    .output(item('extendedcrafting:singularity_custom:69'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Taranium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockTaranium'))
    .inputCount(1215)
    .output(item('extendedcrafting:singularity_custom:70'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Potin Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockPotin'))
    .inputCount(1824)
    .output(item('extendedcrafting:singularity_custom:71'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Obsidian Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockObsidian'))
    .inputCount(229)
    .output(item('extendedcrafting:singularity_custom:72'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Tiberium Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockTiberium'))
    .inputCount(304)
    .output(item('extendedcrafting:singularity_custom:73'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Cobalt Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockCobalt'))
    .inputCount(1824)
    .output(item('extendedcrafting:singularity:65'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Ender Pearl Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockEnderPearl'))
    .inputCount(608)
    .output(item('extendedcrafting:singularity_custom:74'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// Fluix Singularity
mods.extendedcrafting.compression_crafting.recipeBuilder()
    .input(ore('blockFluix'))
    .inputCount(380)
    .output(item('extendedcrafting:singularity_custom:75'))
    .catalyst(item('extendedcrafting:material:13')) // Ultimate Catalyst
    .consumeCatalyst(true)
    .powerCost(2147483647) // Total Energy Consumed: 2,147,483,647 RF
    .powerRate(4194304) // 4,194,304 RF/tick (1,048,576 GTEU/tick, UHV), almost 512 tick (25 sec).
    .register()

// TODO Chronic Singularity Component small singularities.